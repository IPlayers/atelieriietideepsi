MathHero from https://github.com/Glank/Java-Games

The problems will come at you. Type the answer and hit enter or they'll kill you.
When you die, you can use the key in the top right to return to the same level. Just type it once you start a new game, then hit enter.

URL for unit tests coverage : 
https://gitlab.com/IPlayers/atelieriietideepsi/-/jobs/artifacts/master/file/build/reports/jacoco/test/html/index.html?job=coverage